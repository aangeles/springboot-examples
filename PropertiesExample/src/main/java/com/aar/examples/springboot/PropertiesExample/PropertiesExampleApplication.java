package com.aar.examples.springboot.PropertiesExample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PropertiesExampleApplication  implements CommandLineRunner{

	@Autowired
	private ExampleConfig cfg;
	
		
	public static void main(String[] args) {
		SpringApplication.run(PropertiesExampleApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		
		System.out.println("=============== Run ============");
		System.out.println(cfg);			
	}

}
